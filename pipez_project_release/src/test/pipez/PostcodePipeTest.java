
package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import static pipez.util.TestUtils.*;

public class PostcodePipeTest{

	@Test
	public void test_validpostcode() {
		SimpleBlock sb = new SimpleBlock("1234","2134");
		PostcodePipe select = PostcodePipe.create("2134");
		Block b = select.transform(sb);
		assertThat(b.values().length, is(2));
		assertThat(b.values(), are("1234","2134"));
		
	}

	

	

}

