package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import static pipez.util.TestUtils.*;

public class  cryptionTest{

	@Test
	public void test_encryption() {
		SimpleBlock sb = new SimpleBlock("abd","dee");
		EncryptionPipe select = EncryptionPipe.create("abc");
		Block b = select.transform(sb);
		assertThat(b.values().length, is(2));
		assertThat(b.values(), are("zyw","wvu"));
		
	}
	public void test_decryption(){
		SimpleBlock sb = new SimpleBlock("abc","def");
		DecryptionPipe select = DecryptionPipe.create("abc");
		Block b = select.transform(sb);
		assertThat(b.values().length, is(2));
		assertThat(b.values(), are("zyx","wvu"));
		
		
	}
	

	

}

