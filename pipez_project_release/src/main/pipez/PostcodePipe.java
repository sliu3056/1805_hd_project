package pipez;
import static pipez.core.SpecialBlocks.SKIP_BLOCK;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.SimpleBlock;
public class PostcodePipe {
	public String getName() {
		return "postcode";
	}

		private String code = null;
		
		private PostcodePipe(String code) {
			this.code = code;

		}

		
		public static PostcodePipe create(String code) {
			return new PostcodePipe(code);
		}
		
		public Block transform(Block block) {
			SimpleBlock newBlock = new SimpleBlock();
			
			int i=0;
			String postcode;
		//	String[] fields = block.fields();
			for(String v :block.values()) {
			 char[] name = new char[v.length()];
              for(i=0;i<v.length();i++ ){
            	  name[i]=v.charAt(i);
              }
            
             postcode=String.valueOf(check(name));
             newBlock.add(postcode);
            i++;

			}
			
			return newBlock;
		}
		
		 public char[] check(char[] code){
			 char [] value={'i','n','v','a','l','i','d'};
			  if(code.length!=4){
				  return value;
			  }
			  else{ 
		        for(int i=0;i<4;i++)
		        {
		         if(code[i]<48||code[i]>57){
		        	 return value;
		         }	 
		        }
		      return code;	  
			  }	  
		    	  
		 }
		 
		
		
}